package uk.nhs.nhsbsa.time.age;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.joda.time.LocalDate;
import org.joda.time.Period;

public class LocalDateAgeConstraintValidator implements ConstraintValidator<Age, LocalDate> {

    private DateAgeValidator delegate;

    @Override
    public void initialize(Age age) {
        delegate = new DateAgeValidator(age.comparison(), Period.parse(age.threshold()));
    }

    @Override
    public boolean isValid(LocalDate value, ConstraintValidatorContext context) {
        boolean valid = true;
        if (value != null) {
            valid = delegate.isValid(value);
        }
        return valid;
    }
}
