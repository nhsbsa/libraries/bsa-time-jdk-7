package uk.nhs.nhsbsa.time.age;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static uk.nhs.nhsbsa.time.test.ConstraintValidator.isValid;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.junit.Test;

public abstract class AbstractDateAgeConstraintValidatorTest<F> {

    @Test
    public void isValidWhenFieldNull() {
        assertTrue(isValid(emptyFixture()));
    }

    @Test
    public void isValidWhenFieldEmpty() {
        assertTrue(isValid(fixture(null)));
    }

    @Test
    public void isValidWhenAgeEqOneYear() {
        assertTrue(isValid(fixture("P-1Y")));
    }

    @Test
    public void isValidWhenAgeEqOneYearOneDay() {
        assertTrue(isValid(fixture("P-1Y-1D")));
    }

    @Test
    public void isInvalidWhenAgeEqTw0Year() {
        assertFalse(isValid(fixture("P-2Y")));
    }

    private F fixture(String period) {

        F fixture = emptyFixture();
        if (period != null) {
            Period p = Period.parse(period);
            LocalDate d = LocalDate.now().plus(p);
            setDate(fixture, d);
        }
        return fixture;
    }

    protected abstract F emptyFixture();

    protected abstract void setDate(F fixture, LocalDate date);

}
