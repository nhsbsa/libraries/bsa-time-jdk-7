package uk.nhs.nhsbsa.time.util;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import uk.nhs.nhsbsa.time.util.ObjectUtils;

public class ObjectUtilsTest {

    @Test
    public void anyNullReturnsTrueWhenNoArgs() {
        assertThat(ObjectUtils.anyNull()).isFalse();
    }

    @Test
    public void anyNullReturnsTrueWhenMixedNulls() {
        assertThat(ObjectUtils.anyNull("one", null)).isTrue();
    }

    @Test
    public void anyNullReturnsFalseWhenNotNull() {
        assertThat(ObjectUtils.anyNull("one")).isFalse();
    }

    @Test
    public void allNullReturnsFalseWhenNoArgs() {
        assertThat(ObjectUtils.allNull()).isFalse();
    }

    @Test
    public void allNullReturnsFalseWhenMixedNulls() {
        assertThat(ObjectUtils.allNull("one", null)).isFalse();
    }

    @Test
    public void allNullReturnsTrueWhenNulls() {
        assertThat(ObjectUtils.allNull(null, null)).isTrue();
    }

    @Test
    public void allNullReturnsFalseWhenNotNull() {
        assertThat(ObjectUtils.allNull("one")).isFalse();
    }
}
